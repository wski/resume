(function(){
    let Environment = {
        //mobile or desktop compatible event name, to be used with '.on' function
        TOUCH_DOWN_EVENT_NAME: 'mousedown touchstart',
        TOUCH_UP_EVENT_NAME: 'mouseup touchend',
        TOUCH_MOVE_EVENT_NAME: 'mousemove touchmove',
        TOUCH_DOUBLE_TAB_EVENT_NAME: 'dblclick dbltap',

        isAndroid: function() {
            return navigator.userAgent.match(/Android/i);
        },
        isBlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        isIOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        isOpera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        isWindows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        isMobile: function() {
            return (Environment.isAndroid() || Environment.isBlackBerry() || Environment.isIOS() || Environment.isOpera() || Environment.isWindows());
        }
    };

    let getAge = function(dateString) {
        let today = new Date();
        let birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    let content = {
        age : getAge('09/07/1992'),
        experience : getAge('09/07/2006'),
        projects : 50
    };

    let typetastic = {
        settings: {
            speed: 100,
            pauseTime: 2000
        },
        content: [
            'NodeJS',
            'React',
            'Less',
            'Sass',
            'Bower',
            'Grunt',
            'JavaScript',
            'Photoshop',
            'Illustrator',
            'UI/UX',
            '*nix',
            'ES6',
            'MongoDB'
        ]
    }

    let materialColors = ['#F44336','#E91E63','#9C27B0','#673AB7','#3F51B5'];

    let typetasticStart = function(){
        let new_word = (function(){
            let current_length = 0;
            let reverse = false;
            let last_word = '';
            let chosen_word = typetastic.content[Math.floor(Math.random() * typetastic.content.length)];
            let addLetter = function(){
                if(current_length >= chosen_word.length){
                    reverse = true;
                }

                if(!reverse){
                    current_length++;
                    $('#typetastic').html(chosen_word.substring(0, current_length));
                    if(current_length === chosen_word.length){
                        $('#cursor').addClass('blink');
                        setTimeout(function(){
                            $('#cursor').removeClass('blink');
                            addLetter();
                        }, typetastic.settings.pauseTime);
                    }else{
                        setTimeout(addLetter, typetastic.settings.speed);
                    }
                }
                if(reverse){
                    current_length--;
                    $('#typetastic').html(chosen_word.substring(0, current_length));
                    setTimeout(addLetter, typetastic.settings.speed);
                    if(current_length === 0){
                        reverse = false;
                        last_word = chosen_word;
                        chosen_word = typetastic.content[Math.floor(Math.random() * typetastic.content.length)];
                        if(chosen_word === last_word){
                            chosen_word = typetastic.content[Math.floor(Math.random() * typetastic.content.length)];
                        }
                    }
                }
            }
            addLetter();
        })();
    };

    let hasRunProgress = false;
    let animateProgress = function(){
        if(!hasRunProgress){
            hasRunProgress = true;
            $('.progress-bar').each((index, item) => $(item).animate({width: $(item).data('width')}, 500));
        }
    };

    $(document).ready(function(){
        jQuery(function($) {
            var _oldShow = $.fn.show;

            $.fn.show = function(speed, oldCallback) {
            return $(this).each(function() {
              var obj         = $(this),
                  newCallback = function() {
                    if ($.isFunction(oldCallback)) {
                      oldCallback.apply(obj);
                    }
                    obj.trigger('afterShow');
                  };

              // you can trigger a before show if you want
              obj.trigger('beforeShow');

              // now use the old function to show the element passing the new callback
              _oldShow.apply(obj, [speed, newCallback]);
            });
            }
        });
        // Replace variables
        $('v').each((index, item) => $(item).html( content[$(item).data('var')] ));

        // Start the typing animation
        typetasticStart();

        // Randomize progress bar colors and animate
        $('.progress-bar').each((index, item) => $(item).css('background-color', materialColors[ Math.floor(Math.random() * materialColors.length) ]));

        if(Environment.isMobile) animateProgress();
        $(window).scroll(function(){
            if($('#progress_trigger').is(':visible')) animateProgress();
        });

    });
})();

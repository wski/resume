var gulp = require('gulp');
var babel = require('gulp-babel');
var less = require('gulp-less');
var path = require('path');

gulp.task('copy-html', function() {
    return gulp.src([
        'src/*.html'
        ])
        .pipe(gulp.dest('dist'));
});

gulp.task('copy-img', function() {
    return gulp.src([
        'src/img/*'
        ])
        .pipe(gulp.dest('dist/img'));
});

gulp.task('babel', function () {
    return gulp.src('src/index.js')
        .pipe(babel())
        .pipe(gulp.dest('dist'));
});

gulp.task('less', function () {
  return gulp.src('./src/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('default', ['babel', 'less', 'copy-html', 'copy-img']);
